Django Tutorial

Prerequisite:

    - GNU Make: https://www.gnu.org/software/make/

Get Started

    1. Use `make init` command to build docker images and its related containers
        - container: db-1 is a docker container for PostgreSQL
        - container: web-1 is a dockeer container for django

    2. Use `make start` command to start the containers

    3. Use `make migrate` command to start database migrations

    4. Use `make init_user` command to create superuser

    5. Use `make stop` command to the containers

    6. Use `make restart` command to restart the containers
    
    7. Use `make cleanup` command to delete the containers, images and volume
